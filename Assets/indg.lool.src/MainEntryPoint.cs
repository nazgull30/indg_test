﻿using indg.lool.src.utils;
using Ninject;
using Uniject.Impl;
using UnityEngine;

namespace indg.lool.src
{
	public class MainEntryPoint : MonoBehaviour
	{
		public NguiDataContext Root;

		private IKernel _kernel;

		private void Awake ()
		{
			_kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());

			var uiContext = _kernel.Get<UiContext> ();
			Root.SetContext (uiContext);

			uiContext.Menu.Visible = true;
		}

		private void OnDestroy ()
		{
			_kernel.Dispose ();
		}

		private void OnApplicationQuit ()
		{
			_kernel.Dispose ();
		}
	}
}