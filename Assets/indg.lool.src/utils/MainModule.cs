using Ninject.Modules;
using indg.lool.src.game;
using indg.lool.src.viewmodels;

namespace indg.lool.src.utils
{
	public class MainModule : NinjectModule
	{
		public override void Load ()
		{
			Bind<Table> ().ToSelf ().WithConstructorArgument ("size", 10);
			Bind<Coin> ().ToSelf ().WithConstructorArgument ("radius", 1);

			Bind<Game> ().ToSelf ()
								  .WithConstructorArgument ("initialCoinAmount", 20)
								  .WithConstructorArgument ("maxRemovedNumbers", 3);
			Bind<UiContext> ().ToSelf ().InSingletonScope ();
			Bind<GameViewModel> ().ToSelf ().InSingletonScope ();
			Bind<ResultViewModel> ().ToSelf ().InSingletonScope ();
			Bind<MenuViewModel> ().ToSelf ().InSingletonScope ();
			Bind<LeaveGamePopup> ().ToSelf ().InSingletonScope ();
		}
	}
}