using System;
using System.Collections.Generic;
using HipsterCastle.Utils.UnityLogger;
using indg.lool.src.viewmodels;
using SimpleGelf.GraylogLogger;
using SimpleLogger;
using indg.lool.src.game;

namespace indg.lool.src.utils
{
	public static class LoggerConfigurator
	{
		//const string logServerHost = "https://graylog.heroesrage.com:443/gelf";

		private static readonly LogServerHandler _logServerHandler = new LogServerHandler ();

		private static bool _configured;

		public static void ConfigureLogger ()
		{
			if (_configured)
				return;
			_configured = true;

			//SimpleLogger.Logger.LoggerHandlerManager.AddHandler (_logServerHandler.Configure (logServerHost));
			Logger.LoggerHandlerManager.AddHandler (new UnityConsoleHandler ());
			AddClasses ();
		}

		private static void AddClasses ()
		{
			Logger.Configurator
			      .AddClass<Game> ()
				  .AddClass<GameViewModel> ();
		}

		public static void ActivateLogs ()
		{
			AddClasses ();
		}

		public static void LoadClasses (List<string> classNames)
		{
			foreach (var className in classNames) {
				var type = Type.GetType (className);
				Logger.Configurator.AddClass (type);
			}
		}


		public static void SetGameCenterId (string gcId)
		{
			_logServerHandler.SetGameCenterId (gcId);
		}

		public static void SetUserInfo (int userId, string username, long facebookId)
		{
			_logServerHandler.SetUserInfo (userId, username, facebookId);
		}
	}
}