using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace indg.lool.src.utils
{
	/// <summary>
	/// Simple script that offsets button when it gets pressed.
	/// </summary>
	public class FlatUIButtonOffset : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		public Transform Target;
		public Vector3 offset = new Vector3 (2, -2, 0);

		public void OnPointerDown (PointerEventData eventData)
		{
			Target.localPosition = Target.localPosition + offset;
		}

		public void OnPointerUp (PointerEventData eventData)
		{
			Target.localPosition = Target.localPosition - offset;
		}
	}
}