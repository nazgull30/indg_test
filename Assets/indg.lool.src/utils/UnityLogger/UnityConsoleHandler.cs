﻿using SimpleLogger.Logging;
using SimpleLogger.Logging.Formatters;
using UnityEngine;

namespace HipsterCastle.Utils.UnityLogger
{
	public class UnityConsoleHandler : ILoggerHandler 
	{
		private readonly ILoggerFormatter _loggerFormatter;

		public UnityConsoleHandler() : this(new DefaultLoggerFormatter()) { }

		public UnityConsoleHandler(ILoggerFormatter loggerFormatter)
		{
			_loggerFormatter = loggerFormatter;
		}

		public void Publish(LogMessage logMessage)
		{
			if (logMessage.Level == SimpleLogger.Logger.Level.Error) {
				Debug.LogError (_loggerFormatter.ApplyFormat (logMessage));
			} else {
				Debug.Log (_loggerFormatter.ApplyFormat (logMessage));
			}
		}
	}
}
