using System.Text;
using SimpleLogger.Logging;
using SimpleLogger.Logging.Formatters;

namespace HipsterCastle.Utils.UnityLogger
{
	public class UnityConsoleFormatter : ILoggerFormatter
	{
		readonly StringBuilder _builder = new StringBuilder();
		readonly string _format = "{0:dd.MM.yyyy HH:mm:ss}: {1} [line: {2} {3} -> {4}()]: {5}";

		string _outputFormat;

		public string ApplyFormat(LogMessage logMessage)
		{
			_builder.Length = 0;

			_outputFormat = !string.IsNullOrEmpty (logMessage.Color) ? 
				_builder.Append ("<color=#").Append (logMessage.Color).Append (">").Append (_format).Append ("</color>").ToString ()
				: _format;
			
			return string.Format(_outputFormat,
				logMessage.DateTime, logMessage.Level, logMessage.LineNumber, logMessage.CallingClass,
				logMessage.CallingMethod, logMessage.Text);
		}
	}
}

