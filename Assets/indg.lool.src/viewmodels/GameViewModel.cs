﻿using indg.lool.src.game;
using Ninject;
using System.Linq;

namespace indg.lool.src.viewmodels
{
	public class GameViewModel : EZData.Context
	{
		private readonly ResultViewModel _resultVm;
		private readonly LeaveGamePopup _leaveGamePopup;
		private readonly IKernel _kernel;

		private Game _game;

		public GameViewModel (ResultViewModel resultVm, LeaveGamePopup leaveGamePopup, IKernel kernel)
		{
			_resultVm = resultVm;
			_leaveGamePopup = leaveGamePopup;
			_kernel = kernel;
		}

		public void ConfigureAiGame ()
		{
			var mode = indg.lool.src.game.Mode.Ai;
			var firstPlayer = new Player (1, false);
			var secondPlayer = new Player (2, true);
			_game = _kernel.Get<Game> ();
			_game.Configure (firstPlayer, secondPlayer, mode);
			TotalCoins = _game.TotalCoins;
			Mode = (int)mode;
		}

		public void ConfigureFriendGame ()
		{
			var mode = indg.lool.src.game.Mode.Friend;
			var firstPlayer = new Player (1, false);
			var secondPlayer = new Player (2, false);
			_game = _kernel.Get<Game> ();
			_game.Configure (firstPlayer, secondPlayer, mode);
			TotalCoins = _game.TotalCoins;
			Mode = (int)mode;
		}

		public void StartGame ()
		{
			Visible = true;
			_game.OnTurnStarted += OnTurnStarted;
			_game.OnTurnCompleted += OnTurnCompleted;
			_game.OnOver += OnGameOver;
			var playerId = UnityEngine.Random.Range (1, 3); // choose player id 1 or 2
			_game.StartGame (playerId);
			CurrentPlayerId = _game.CurrentPlayerId;
			RealPlayerHasTurn = PlayerIsReal (_game.CurrentPlayerId);
		}

		public void Clear ()
		{
			_game.OnTurnStarted -= OnTurnStarted;
			_game.OnTurnCompleted -= OnTurnCompleted;
			_game.OnOver -= OnGameOver;
			_game.Clear ();
		}

		void OnTurnStarted (int currentPlayerId)
		{
			CurrentPlayerId = _game.CurrentPlayerId;
			RealPlayerHasTurn = PlayerIsReal (_game.CurrentPlayerId);
		}

		void OnTurnCompleted ()
		{
			SimpleLogger.Logger.Log<GameViewModel> ("OnTurnCompleted, CurrentPlayerId: " + CurrentPlayerId);
			TotalCoins = _game.TotalCoins;
		}

		void OnGameOver (Player winner)
		{
			var turns = _game.Turns.Reverse ().ToArray ();
			var result = new Result (winner, turns);
			Visible = false;
			_resultVm.Show (result, _game.Mode);
			Clear ();
		}

		#region Button Handlers

		public void LeaveBtn ()
		{
			_leaveGamePopup.Show (() => {
				Visible = false;
				Clear ();
			});
			// use confirm popup
		}

		public void RemoveOneCoinBtn ()
		{
			_game.MakeTurn (1);
		}

		public void RemoveTwoCoinsBtn ()
		{
			_game.MakeTurn (2);
		}


		public void RemoveThreeCoinsBtn ()
		{
			_game.MakeTurn (3);
		}

		public bool PlayerIsReal (int playerId)
		{
			return !_game.GetPlayerById (playerId).IsAi;
		}

		#endregion

		#region Property  TotalCoins
		private readonly EZData.Property<int> _privateTotalCoinsProperty = new EZData.Property<int> ();
		public EZData.Property<int> TotalCoinsProperty { get { return _privateTotalCoinsProperty; } }
		public int TotalCoins {
			get { return TotalCoinsProperty.GetValue (); }
			set { TotalCoinsProperty.SetValue (value); }
		}
		#endregion

		#region Property  RealPlayerHasTurn
		private readonly EZData.Property<bool> _privateRealPlayerHasTurnProperty = new EZData.Property<bool> ();
		public EZData.Property<bool> RealPlayerHasTurnProperty { get { return _privateRealPlayerHasTurnProperty; } }
		public bool RealPlayerHasTurn {
			get { return RealPlayerHasTurnProperty.GetValue (); }
			set { RealPlayerHasTurnProperty.SetValue (value); }
		}
		#endregion

		#region Property  CurrentPlayerId
		private readonly EZData.Property<int> _privateCurrentPlayerIdProperty = new EZData.Property<int> ();
		public EZData.Property<int> CurrentPlayerIdProperty { get { return _privateCurrentPlayerIdProperty; } }
		public int CurrentPlayerId {
			get { return CurrentPlayerIdProperty.GetValue (); }
			set { CurrentPlayerIdProperty.SetValue (value); }
		}
		#endregion

		#region Property  Visible
		private readonly EZData.Property<bool> _privateVisibleProperty = new EZData.Property<bool> (false);
		public EZData.Property<bool> VisibleProperty { get { return _privateVisibleProperty; } }
		public bool Visible {
			get { return VisibleProperty.GetValue (); }
			set { VisibleProperty.SetValue (value); }
		}
		#endregion

		#region Property  Mode
		private readonly EZData.Property<int> _privateModeProperty = new EZData.Property<int> ();
		public EZData.Property<int> ModeProperty { get { return _privateModeProperty; } }
		public int Mode {
			get { return ModeProperty.GetValue (); }
			set { ModeProperty.SetValue (value); }
		}
		#endregion
	}
}