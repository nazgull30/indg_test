﻿using System;
using UnityEngine;
using indg.lool.src.game;

namespace indg.lool.src.viewmodels
{
	public class MenuViewModel : EZData.Context
	{
		public Action<Mode> OnModeChosen;

		public void ChooseAiModeBtn ()
		{
			if (OnModeChosen != null) {
				OnModeChosen (Mode.Ai);
			}
			Visible = false;
		}

		public void ChooseFriendModeBtn ()
		{
			if (OnModeChosen != null) {
				OnModeChosen (Mode.Friend);
			}
			Visible = false;
		}

		public void ExitBtn ()
		{
			Application.Quit ();
		}

		#region Property  Visible
		private readonly EZData.Property<bool> _privateVisibleProperty = new EZData.Property<bool> ();
		public EZData.Property<bool> VisibleProperty { get { return _privateVisibleProperty; } }
		public bool Visible {
			get { return VisibleProperty.GetValue (); }
			set { VisibleProperty.SetValue (value); }
		}
		#endregion

	}
}