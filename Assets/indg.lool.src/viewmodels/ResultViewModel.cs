﻿using indg.lool.src.game;
using System;

namespace indg.lool.src.viewmodels
{
	public class ResultViewModel : EZData.Context
	{
		public Action OnContinued;
		public void Show (Result result, Mode mode)
		{
			Visible = true;
			foreach (var turn in result.Turns) {
				Turns.Add (turn);
		
			}
			WinnerId = result.Winner.Id;
			WinnerIsAi = result.Winner.IsAi;
			Mode = (int)mode;
		}

		public void ContinueBtn ()
		{
			Visible = false;
			Turns.Clear ();
			if (OnContinued != null) {
				OnContinued ();
			}
		}

		#region Property  WinnerId
		private readonly EZData.Property<int> _privateWinnerIdProperty = new EZData.Property<int> ();
		public EZData.Property<int> WinnerIdProperty { get { return _privateWinnerIdProperty; } }
		public int WinnerId {
			get { return WinnerIdProperty.GetValue (); }
			set { WinnerIdProperty.SetValue (value); }
		}
		#endregion

		#region Property  WinnerIsAi
		private readonly EZData.Property<bool> _privateWinnerIsAiProperty = new EZData.Property<bool> ();
		public EZData.Property<bool> WinnerIsAiProperty { get { return _privateWinnerIsAiProperty; } }
		public bool WinnerIsAi {
			get { return WinnerIsAiProperty.GetValue (); }
			set { WinnerIsAiProperty.SetValue (value); }
		}
		#endregion

		#region Property  Mode
		private readonly EZData.Property<int> _privateModeProperty = new EZData.Property<int> ();
		public EZData.Property<int> ModeProperty { get { return _privateModeProperty; } }
		public int Mode {
			get { return ModeProperty.GetValue (); }
			set { ModeProperty.SetValue (value); }
		}
		#endregion

		#region Collection Turns
		readonly EZData.Collection<Turn> _privateTurns = new EZData.Collection<Turn> ();
		public EZData.Collection<Turn> Turns { get { return _privateTurns; } }
		#endregion

		#region Property  Visible
		private readonly EZData.Property<bool> _privateVisibleProperty = new EZData.Property<bool> (false);
		public EZData.Property<bool> VisibleProperty { get { return _privateVisibleProperty; } }
		public bool Visible {
			get { return VisibleProperty.GetValue (); }
			set { VisibleProperty.SetValue (value); }
		}
		#endregion

	}

}