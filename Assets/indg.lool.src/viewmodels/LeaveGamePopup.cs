﻿using System;

namespace indg.lool.src.viewmodels
{
	public class LeaveGamePopup : EZData.Context
	{
		public Action OnConfirmed;

		private Action _ok;
		public void Show (Action ok)
		{
			_ok = ok;
			Visible = true;
		}

		public void LeaveBtn ()
		{
			if(_ok != null) {
				_ok ();
			}
			_ok = null;
			if (OnConfirmed != null) {
				OnConfirmed ();
			}

			Visible = false;
		}

		public void ContinueBtn ()
		{
			Visible = false;
			_ok = null;
		}

		#region Property  Visible
		private readonly EZData.Property<bool> _privateVisibleProperty = new EZData.Property<bool> (false);
		public EZData.Property<bool> VisibleProperty { get { return _privateVisibleProperty; } }
		public bool Visible {
			get { return VisibleProperty.GetValue (); }
			set { VisibleProperty.SetValue (value); }
		}
		#endregion

	}
}