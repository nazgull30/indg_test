﻿namespace indg.lool.src.game
{
	public class Turn : EZData.Context
	{

		public Turn (int number, int playerId, int removedCoinAmount, int endCoinAmount, Mode mode)
		{
			Number = number;
			PlayerId = playerId;
			RemovedCoinAmount = removedCoinAmount;
			EndCoinAmount = endCoinAmount;
			Mode = (int)mode;
		}

		#region Property  Number
		private readonly EZData.Property<int> _privateNumberProperty = new EZData.Property<int> ();
		public EZData.Property<int> NumberProperty { get { return _privateNumberProperty; } }
		public int Number {
			get { return NumberProperty.GetValue (); }
			set { NumberProperty.SetValue (value); }
		}
		#endregion


		#region Property  RemovedCoinAmount
		private readonly EZData.Property<int> _privateRemovedCoinAmountProperty = new EZData.Property<int> ();
		public EZData.Property<int> RemovedCoinAmountProperty { get { return _privateRemovedCoinAmountProperty; } }
		public int RemovedCoinAmount {
			get { return RemovedCoinAmountProperty.GetValue (); }
			set { RemovedCoinAmountProperty.SetValue (value); }
		}
		#endregion

		#region Property  EndCoinAmount
		private readonly EZData.Property<int> _privateEndCoinAmountProperty = new EZData.Property<int> ();
		public EZData.Property<int> EndCoinAmountProperty { get { return _privateEndCoinAmountProperty; } }
		public int EndCoinAmount {
			get { return EndCoinAmountProperty.GetValue (); }
			set { EndCoinAmountProperty.SetValue (value); }
		}
		#endregion

		#region Property  PlayerId
		private readonly EZData.Property<int> _privatePlayerIdProperty = new EZData.Property<int> ();
		public EZData.Property<int> PlayerIdProperty { get { return _privatePlayerIdProperty; } }
		public int PlayerId {
			get { return PlayerIdProperty.GetValue (); }
			set { PlayerIdProperty.SetValue (value); }
		}
		#endregion


		#region Property  Mode
		private readonly EZData.Property<int> _privateModeProperty = new EZData.Property<int> ();
		public EZData.Property<int> ModeProperty { get { return _privateModeProperty; } }
		public int Mode {
			get { return ModeProperty.GetValue (); }
			set { ModeProperty.SetValue (value); }
		}
		#endregion


	}
}