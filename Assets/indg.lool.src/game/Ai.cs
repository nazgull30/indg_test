﻿using System.Collections.Generic;
using UnityEngine;

namespace indg.lool.src.game
{
	public class Ai
	{
		private readonly List<int> _deathNumbers = new List<int> ();
		private int _maxRemovedNumbers;

		public void Configure (int maxRemovedNumbers, int maxTotalCoins)
		{
			_maxRemovedNumbers = maxRemovedNumbers;
			for (int k = 0; k < maxTotalCoins; k++) {
				var deathNumber = (maxRemovedNumbers + 1) * k + 1;
				_deathNumbers.Add (deathNumber);
			}

		}

		public int CalculateCoinAmountToBeRemoved (int currentTotalCoins)
		{
			for (int i = currentTotalCoins; i > 0; i--) {
				if (_deathNumbers.Contains (i)) {
					var diff = currentTotalCoins - i;
					if (diff > 0 && diff <= _maxRemovedNumbers) {
						return diff;
					}
					break;
				}
			}
			return Random.Range (1, _maxRemovedNumbers + 1);
		}
	}
}
