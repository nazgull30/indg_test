﻿using System;
using System.Collections.Generic;
using UniRx;

namespace indg.lool.src.game
{
	public enum Mode
	{
		Ai, Friend
	}


	public class Game
	{
		public Action OnTurnCompleted;
		public Action<int> OnTurnStarted;
		public Action<Player> OnOver;

		private readonly Ai _ai;

		private readonly Table _table;

		private readonly Stack<Turn> _turns = new Stack<Turn> ();

		public Player FirstPlayer { get; private set; }
		public Player SecondPlayer { get; private set; }
		public Mode Mode { get; private set; }

		private readonly int _initialCoinAmount;

		private int _currentPlayerId;

		private IDisposable _disposable = Disposable.Empty;

		public Game (Ai ai, Table table, int maxRemovedNumbers, int initialCoinAmount)
		{
			_ai = ai;
			_table = table;
			_initialCoinAmount = initialCoinAmount;
			_ai.Configure (maxRemovedNumbers, initialCoinAmount);
		}

		public void Configure (Player firstPlayer, Player secondPlayer, Mode mode)
		{
			FirstPlayer = firstPlayer;
			SecondPlayer = secondPlayer;
			Mode = mode;

			_table.InitCoins (_initialCoinAmount);
		}

		public void StartGame (int playerId)
		{
			_currentPlayerId = playerId;
			var currentPlayer = GetPlayerById (playerId);

			if (OnTurnStarted != null) {
				OnTurnStarted (_currentPlayerId);
			}

			if (currentPlayer.IsAi) {
				InternalMakeTurnByAi ();
			}
		}

		public void MakeTurn (int removedCoins)
		{
			SimpleLogger.Logger.Log<Game> ("MakeTurn -> removedCoins:" + removedCoins + ", _currentPlayerId: " + _currentPlayerId);
			_table.RemoveCoins (removedCoins);

			CompleteTurn (_currentPlayerId, removedCoins);

			if (TryOver ()) return;

			_currentPlayerId = GetNextPlayerId ();
			var currentPlayer = GetPlayerById (_currentPlayerId);

			if (OnTurnStarted != null) {
				OnTurnStarted (_currentPlayerId);
			}

			if (currentPlayer.IsAi) {
				InternalMakeTurnByAi ();
			}
		}

		private void InternalMakeTurnByAi ()
		{
			if (UnityEngine.Application.isPlaying) { //for unit tests
				_disposable = Observable.TimerFrame (60).Subscribe (_ => {
					MakeTurnByAi ();
				});
			} else {
				MakeTurnByAi ();
			}
		}

		private void MakeTurnByAi ()
		{
			_disposable.Dispose ();
			var removedCoins = _ai.CalculateCoinAmountToBeRemoved (TotalCoins);
			SimpleLogger.Logger.Log<Game> ("MakeTurnByAi -> ai remove coins " + removedCoins);
			MakeTurn (removedCoins);
		}

		private bool TryOver ()
		{
			if (TotalCoins > 0) return false;
			Winner = GetPlayerById (GetNextPlayerId ()); // other player is winner
			IsOver = true;
			if (OnOver != null) {
				OnOver (Winner);
			}
			return true;
		}

		private void CompleteTurn (int playerId, int removedCoins)
		{
			var turn = new Turn (_turns.Count + 1, playerId, removedCoins, TotalCoins, Mode);
			_turns.Push (turn);

			SimpleLogger.Logger.Log<Game> ("CompleteTurn -> playerId: " + playerId + ", remain coins: " + TotalCoins);

			if (OnTurnCompleted != null) {
				OnTurnCompleted ();
			}
		}

		public Player GetPlayerById (int id)
		{
			if (FirstPlayer.Id == id)
				return FirstPlayer;
			if (SecondPlayer.Id == id)
				return SecondPlayer;
			return null;
		}

		private int GetNextPlayerId ()
		{
			return _turns.Peek ().PlayerId == 1 ? 2 : 1;
		}

		public void Clear ()
		{
			_turns.Clear ();
			_table.Destroy ();
			IsOver = false;
			Winner = null;
		}

		public int TotalCoins { get { return _table.TotalCoins; } } 

		public int CurrentPlayerId { get { return _currentPlayerId; } }

		public bool IsOver { private set; get; }

		public Stack<Turn> Turns { get { return _turns; } }

		public Player Winner { private set; get; }

	}
}
