﻿namespace indg.lool.src.game
{
	public class Result
	{
		public Player Winner { get; private set; }
		public Turn[] Turns { get; private set; }

		public Result (Player winner, Turn [] turns)
		{
			Winner = winner;
			Turns = turns;
		}
	}
}