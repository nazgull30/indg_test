﻿using UnityEngine;
using System.Collections.Generic;
using Ninject;
using Uniject;

namespace indg.lool.src.game
{

	public class Table : TestableComponent
	{
		private readonly IList<Coin> _coins = new List<Coin> ();
		private readonly IKernel _kernel;

		private readonly float _halfSize;

		public Table ([Resource ("table")] ITestableGameObject obj, IKernel kernel, float size) : base (obj)
		{
			_kernel = kernel;
			_halfSize = size * 0.5f;
		}

		public void InitCoins (int amount)
		{
			for (int i = 0; i < amount; i++) {
				var coin = CreateCoin ();
				_coins.Add (coin);
			}
		}

		public void RemoveCoins (int amount)
		{
			var removed = Mathf.Clamp (amount, 0, _coins.Count);
			for (int i = 1; i <= removed; i++) {
				var rndIndex = Random.Range (0, _coins.Count);
				var coin = _coins [rndIndex];
				coin.Destroy ();
				_coins.RemoveAt (rndIndex);
			}
		}

		public void Destroy ()
		{
			foreach (var coin in _coins) {
				coin.Destroy ();
			}
			_coins.Clear ();
			if (Application.isPlaying) {
				Obj.Destroy ();
			}
		}

		private Coin CreateCoin ()
		{
			var coin = _kernel.Get<Coin> ();
			var position = CalcFreePositionForCoin ();
			coin.SetPosition (position);
			return coin;
		}

		public int TotalCoins { get { return _coins.Count; } }


		/// <summary>
		/// Calculates the coin position. Coin cannot overlap other coins.
		/// THIS METHOD IS NOT SAFE!!!!! 
		/// Table size and coin radius should be adjusted!!
		/// </summary>
		/// <returns>The coin position.</returns>
		private Vector3 CalcFreePositionForCoin ()
		{
			var pos = Vector3.zero;
			while (pos.Equals (Vector3.zero)) {
				var x = Random.Range (-_halfSize, _halfSize);
				var z = Random.Range (-_halfSize, _halfSize);
				var position = new Vector3 (x, 0, z);

				var check = true;
				foreach (var coin in _coins) {
					if ((coin.Position - position).sqrMagnitude < coin.Radius * coin.Radius + 0.5f)  {
						check = false;
						break;
					}
				}

				if (!check)
					continue;
				pos = position;
			}

			return pos;
		}
	}
}
