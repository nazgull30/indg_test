﻿namespace indg.lool.src.game
{
	public class Player
	{
		public int Id { get; private set; }
		public bool IsAi { get; private set; }

		public Player (int id, bool isAi)
		{
			Id = id;
			IsAi = isAi;
		}
	}
}
