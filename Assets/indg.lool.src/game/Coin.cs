﻿using UnityEngine;
using Uniject;

namespace indg.lool.src.game
{
	public class Coin : TestableComponent
	{
		private readonly float _radius;

		public Coin ([Resource("coin")] ITestableGameObject obj, float radius) : base (obj)
		{
			_radius = radius;
		}

		public void SetPosition (Vector3 position)
		{
			Obj.Transform.Position = position;
		}

		public Vector3 Position { get { return Obj.Transform.Position; } }

		public float Radius { get { return _radius; } } 

		public void Destroy ()
		{
			if (Application.isPlaying) {
				Obj.Destroy ();
			}
		}
	}

}