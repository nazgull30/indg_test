﻿using indg.lool.src.viewmodels;
using Ninject;
using System;

namespace indg.lool.src
{
	public class UiContext : EZData.Context, IInitializable, IDisposable
	{

		public void Initialize ()
		{
			Menu.OnModeChosen += Menu_OnModeChosen;
			ResultViewModel.OnContinued += ResultViewModel_OnContinued;
			LeaveGamePopup.OnConfirmed += LeaveGamePopup_OnConfirmed;
		}

		public void Dispose ()
		{
			Menu.OnModeChosen -= Menu_OnModeChosen;
			ResultViewModel.OnContinued -= ResultViewModel_OnContinued;
			LeaveGamePopup.OnConfirmed -= LeaveGamePopup_OnConfirmed;
		}

		void Menu_OnModeChosen (game.Mode mode)
		{
			switch (mode) {
				case game.Mode.Ai: 
					GameViewModel.ConfigureAiGame ();
				break;
				case game.Mode.Friend:
					GameViewModel.ConfigureFriendGame ();
				break;
			}
			GameViewModel.StartGame ();
		}

		private void ResultViewModel_OnContinued ()
		{
			GameViewModel.Visible = false;
			Menu.Visible = true;
		}

		void LeaveGamePopup_OnConfirmed ()
		{
			Menu.Visible = true;
		}

		#region  Menu
		public readonly EZData.VariableContext<MenuViewModel> MenuEzVariableContext = new EZData.VariableContext<MenuViewModel> (null);
		[Inject]
		public MenuViewModel Menu {
			get { return MenuEzVariableContext.Value; }
			set { MenuEzVariableContext.Value = value; }
		}
		#endregion

		#region  GameViewModel
		public readonly EZData.VariableContext<GameViewModel> GameViewModelEzVariableContext = new EZData.VariableContext<GameViewModel> (null);
		[Inject]
		public GameViewModel GameViewModel {
			get { return GameViewModelEzVariableContext.Value; }
			set { GameViewModelEzVariableContext.Value = value; }
		}
		#endregion

		#region  ResultViewModel
		public readonly EZData.VariableContext<ResultViewModel> ResultViewModelEzVariableContext = new EZData.VariableContext<ResultViewModel> (null);
		[Inject]
		public ResultViewModel ResultViewModel {
			get { return ResultViewModelEzVariableContext.Value; }
			set { ResultViewModelEzVariableContext.Value = value; }
		}
		#endregion

		#region  LeaveGamePopup
		public readonly EZData.VariableContext<LeaveGamePopup> LeaveGamePopupEzVariableContext = new EZData.VariableContext<LeaveGamePopup> (null);
		[Inject]
		public LeaveGamePopup LeaveGamePopup {
			get { return LeaveGamePopupEzVariableContext.Value; }
			set { LeaveGamePopupEzVariableContext.Value = value; }
		}
		#endregion

	}
}
