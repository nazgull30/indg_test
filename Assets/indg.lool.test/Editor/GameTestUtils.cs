﻿using indg.lool.src.viewmodels;
using NUnit.Framework;

namespace indg.lool.test
{

	public static class GameTestUtils
	{

		public static void AiGameRealPlayerMakesTurnFirst (GameViewModel gameVm)
		{
			//player removes 1 coins
			gameVm.RemoveOneCoinBtn ();
			//ai should remove N random coins in order to DN = 17
			Assert.AreEqual (gameVm.TotalCoins, 17);

			gameVm.RemoveOneCoinBtn ();
			//ai should remove N coins in order to DN = 13
			Assert.AreEqual (gameVm.TotalCoins, 13);

			gameVm.RemoveOneCoinBtn ();
			//ai should remove N coins in order to DN =  9
			Assert.AreEqual (gameVm.TotalCoins, 9);

			//player removes 1 coin. 8 coins remain
			gameVm.RemoveOneCoinBtn ();
			//ai should remove N coins in order to DN =  5
			Assert.AreEqual (gameVm.TotalCoins, 5);

			//player removes 2 coin. 3 coins remain
			gameVm.RemoveTwoCoinsBtn ();
			//ai should remove N coins in order to DN = 1
			Assert.AreEqual (gameVm.TotalCoins, 1);

			//ai should remove 1 coin. 0 coins remain.
			gameVm.RemoveOneCoinBtn ();

			Assert.False (gameVm.Visible);
		}

		public static void AiGameAiMakesTurnFirst (GameViewModel gameVm)
		{
			//Total count = 17

			//player removes 1 coins. 16 remains
			gameVm.RemoveOneCoinBtn ();
			//ai should remove N coins in order to DN = 13
			Assert.AreEqual (gameVm.TotalCoins, 13);

			gameVm.RemoveOneCoinBtn ();
			//ai should remove N coins in order to DN =  9
			Assert.AreEqual (gameVm.TotalCoins, 9);

			//player removes 1 coin. 8 coins remain
			gameVm.RemoveOneCoinBtn ();
			//ai should remove N coins in order to DN =  5
			Assert.AreEqual (gameVm.TotalCoins, 5);

			//player removes 2 coin. 3 coins remain
			gameVm.RemoveTwoCoinsBtn ();
			//ai should remove N coins in order to DN = 1
			Assert.AreEqual (gameVm.TotalCoins, 1);

			//ai should remove 1 coin. 0 coins remain.
			gameVm.RemoveOneCoinBtn ();

			Assert.False (gameVm.Visible);
		}
	
	}

}