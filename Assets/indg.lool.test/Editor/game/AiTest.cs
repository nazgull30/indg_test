﻿using NUnit.Framework;
using indg.lool.src.game;

namespace indg.lool.test.game
{

	[TestFixture]
	public class AiTest
	{
		private Ai _ai;

		[SetUp]
		public void SetUp ()
		{
			_ai = new Ai ();
			var totalCoins = 20;
			var maxRemovedNumbers = 3;
			_ai.Configure (maxRemovedNumbers, totalCoins);
		}


		[Test]
		public void RemoveCoinAmountWhenCoinsEq20Test ()
		{
			var coins = 20;
			var removedCoins = _ai.CalculateCoinAmountToBeRemoved (coins);
			Assert.AreEqual (removedCoins, 3);
		}

		[Test]
		public void RemoveCoinAmountWhenCoinsEq18Test ()
		{
			var coins = 18;
			var removedCoins = _ai.CalculateCoinAmountToBeRemoved (coins);
			Assert.AreEqual (removedCoins, 1);
		}

		[Test]
		public void RemoveCoinAmountWhenCoinsEq7Test ()
		{
			var coins = 7;
			var removedCoins = _ai.CalculateCoinAmountToBeRemoved (coins);
			Assert.AreEqual (removedCoins, 2);
		}

		[Test]
		public void RemoveCoinAmountWhenCoinsEq5Test ()
		{
			var coins = 5;
			var removedCoins = _ai.CalculateCoinAmountToBeRemoved (coins);
			Assert.True (removedCoins >=1 && removedCoins <= 3);
		}
	}
}