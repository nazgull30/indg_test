﻿using indg.lool.src.utils;
using indg.lool.src.viewmodels;
using Ninject;
using NUnit.Framework;
using Uniject.Impl;

namespace indg.lool.test.game.viewmodels
{
	[TestFixture]
	public class MenuViewModelTest
	{
		private MenuViewModel _menuVm;

		[SetUp]
		public void SetUp ()
		{
			LoggerConfigurator.ConfigureLogger ();
			var kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());
			_menuVm = kernel.Get<MenuViewModel> ();
		}

		[Test]
		public void ChooseAiModeBtnTest ()
		{
			_menuVm.ChooseAiModeBtn ();
			Assert.False (_menuVm.Visible);
		}

		[Test]
		public void ChooseFriendModeBtnTest ()
		{
			_menuVm.ChooseFriendModeBtn ();
			Assert.False (_menuVm.Visible);
		}


	}
}
