﻿using indg.lool.src.utils;
using indg.lool.src.viewmodels;
using Ninject;
using NUnit.Framework;
using Uniject.Impl;

namespace indg.lool.test.game.viewmodels
{
	public class GameViewModelTest
	{
		private GameViewModel _gameVm;

		[SetUp]
		public void SetUp ()
		{
			LoggerConfigurator.ConfigureLogger ();
			var kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());
			_gameVm = kernel.Get<GameViewModel> ();
		}

		[Test]
		public void ConfigureAiGameTest ()
		{
			_gameVm.ConfigureAiGame ();

			//Assert.AreEqual (_gameVm.Config.Mode, Mode.Ai);
			Assert.AreEqual (_gameVm.TotalCoins, 20);
		}

		[Test]
		public void ConfigureFriendGameTest ()
		{
			_gameVm.ConfigureFriendGame ();

			//Assert.AreEqual (_gameVm.Config.Mode, Mode.Friend);
			Assert.AreEqual (_gameVm.TotalCoins, 20);
		}

		[Test]
		public void AiGameProcessTest ()
		{
			ConfigureAiGameTest ();

			_gameVm.StartGame ();

			if (_gameVm.TotalCoins == 20) {//real player makes turn first
				GameTestUtils.AiGameRealPlayerMakesTurnFirst (_gameVm);
			} else {
				GameTestUtils.AiGameAiMakesTurnFirst (_gameVm);
			}
		}

		/// <summary>
		/// Ai should always win!
		/// </summary>
		[Test]
		public void FriendGameProcessTest ()
		{
			ConfigureFriendGameTest ();

			_gameVm.StartGame ();

			//player1 removes 1 coins. 19 coins remain.
			_gameVm.RemoveOneCoinBtn ();

			//player2 removes 1 coins. 18 coins remain.
			_gameVm.RemoveOneCoinBtn ();

			//player1 removes 3 coins. 15 coins remain.
			_gameVm.RemoveThreeCoinsBtn ();

			//player2 removes 3 coins. 12 coins remain.
			_gameVm.RemoveThreeCoinsBtn ();

			//player1 removes 3 coins. 9 coins remain.
			_gameVm.RemoveThreeCoinsBtn ();

			//player2 removes 3 coins. 6 coins remain.
			_gameVm.RemoveThreeCoinsBtn ();

			//player1 removes 3 coins. 3 coins remain.
			_gameVm.RemoveThreeCoinsBtn ();

			//player2 removes 3 coins. 0 coins remain.
			_gameVm.RemoveThreeCoinsBtn ();

			Assert.False (_gameVm.Visible);
		}

	}
}