﻿using indg.lool.src.utils;
using Ninject;
using NUnit.Framework;
using Uniject.Impl;
using indg.lool.src.viewmodels;
using indg.lool.src.game;

namespace indg.lool.test.game.viewmodels
{
	[TestFixture]
	public class ResultViewModelTest 
	{
		private ResultViewModel _resultVm;

		[SetUp]
		public void SetUp ()
		{
			LoggerConfigurator.ConfigureLogger ();
			var kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());
			_resultVm = kernel.Get<ResultViewModel> ();
		}

		[Test]
		public void ShowTest ()
		{
			var winner = new Player (1, false);
			var turns = new Turn [4];
			turns[0] = new Turn (1, 1, 3, 17, Mode.Ai);
			turns[1] = new Turn (2, 2, 3, 14, Mode.Ai);
			turns[2] = new Turn (3, 1, 3, 11, Mode.Ai);
			turns[3] = new Turn (4, 2, 3, 9, Mode.Ai);
			var result = new Result (winner, turns);

			_resultVm.Show (result, Mode.Ai);

			Assert.AreEqual (_resultVm.Turns.Count, 4);
			Assert.True (_resultVm.Visible);
		}

		[Test]
		public void ContinueTest ()
		{
			ShowTest ();

			_resultVm.ContinueBtn ();

			Assert.AreEqual (_resultVm.Turns.Count, 0);
			Assert.False (_resultVm.Visible);
		}
	}

}