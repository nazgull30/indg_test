﻿using NUnit.Framework;
using Ninject;
using indg.lool.src.utils;
using Uniject.Impl;

namespace indg.lool.src.game
{
	[TestFixture]
	public class TableTest
	{
		private Table _table;

		[SetUp]
		public void SetUp ()
		{
			var kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());

			_table = kernel.Get<Table> ();
		}

		[Test]
		public void InitCoinsTest ()
		{
			var totalCoins = 20;
			_table.InitCoins (totalCoins);

			Assert.AreEqual (_table.TotalCoins, totalCoins);
		}

		[Test]
		public void RemoveCoinsTest ()
		{
			InitCoinsTest ();
			var initialCoins = _table.TotalCoins;
			var removedCoins = 3;

			_table.RemoveCoins (removedCoins);

			Assert.AreEqual (initialCoins - _table.TotalCoins, removedCoins);
		}

		[Test]
		public void ClearTest ()
		{
			InitCoinsTest ();
			_table.Destroy ();
			Assert.AreEqual (_table.TotalCoins, 0);
		}

	}
}
