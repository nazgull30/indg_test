﻿using indg.lool.src.game;
using indg.lool.src.utils;
using Ninject;
using NUnit.Framework;
using Uniject.Impl;

namespace indg.lool.test.game
{
	[TestFixture]
	public class GameTest
	{
		private Game _game;

		[SetUp]
		public void SetUp ()
		{
			LoggerConfigurator.ConfigureLogger ();
			var kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());
			_game = kernel.Get<Game> ();
		}

		[Test]
		public void AiGameProcessAiAlwaysWinTest ()
		{
			var mode = Mode.Ai;
			var firstPlayer = new Player (1, false);
			var secondPlayer = new Player (2, true);
			_game.Configure (firstPlayer, secondPlayer, mode);

			var playerId = 1;
			_game.StartGame (playerId);

			//player removes 3 coins
			_game.MakeTurn (1);
			//ai should remove N random coins in order to DN = 17
			Assert.AreEqual (_game.TotalCoins, 17);

			_game.MakeTurn (1);
			//ai should remove N coins in order to DN = 13
			Assert.AreEqual (_game.TotalCoins, 13);

			_game.MakeTurn (1);
			//ai should remove N coins in order to DN =  9
			Assert.AreEqual (_game.TotalCoins, 9);

			//player removes 1 coin. 8 coins remain
			_game.MakeTurn (1);
			//ai should remove N coins in order to DN =  5
			Assert.AreEqual (_game.TotalCoins, 5);

			//player removes 2 coin. 3 coins remain
			_game.MakeTurn (2);
			//ai should remove N coins in order to DN = 1
			Assert.AreEqual (_game.TotalCoins, 1);

			//ai should remove 1 coin. 0 coins remain.
			_game.MakeTurn (1);

			Assert.True (_game.IsOver);
			Assert.True (_game.TotalCoins == 0);
		}

		/// <summary>
		/// Ai should always win!
		/// </summary>
		[Test]
		public void FriendGameProcessTest ()
		{
			var mode = Mode.Friend;
			var firstPlayer = new Player (1, false);
			var secondPlayer = new Player (2, false);
			_game.Configure (firstPlayer, secondPlayer, mode);

			var playerId = 1;
			_game.StartGame (playerId);

			//player1 removes 1 coins. 19 coins remain.
			_game.MakeTurn (1);

			//player2 removes 1 coins. 18 coins remain.
			_game.MakeTurn (1);

			//player1 removes 3 coins. 15 coins remain.
			_game.MakeTurn (3);

			//player2 removes 3 coins. 12 coins remain.
			_game.MakeTurn (3);

			//player1 removes 3 coins. 9 coins remain.
			_game.MakeTurn (3);

			//player2 removes 3 coins. 6 coins remain.
			_game.MakeTurn (3);

			//player1 removes 3 coins. 3 coins remain.
			_game.MakeTurn (3);

			//player2 removes 3 coins. 0 coins remain.
			_game.MakeTurn (3);

			Assert.True (_game.IsOver);
			Assert.AreEqual (_game.Winner.Id, 1);
		}
	}
}
