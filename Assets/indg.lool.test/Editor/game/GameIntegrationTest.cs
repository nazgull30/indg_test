﻿using NUnit.Framework;
using indg.lool.src.viewmodels;
using Ninject;
using indg.lool.src.utils;
using Uniject.Impl;
using indg.lool.src;

namespace indg.lool.test.game
{
	[TestFixture]
	public class GameIntegrationTest
	{
		private MenuViewModel _menuVm;
		private GameViewModel _gameVm;
		private ResultViewModel _resultVm;
		private LeaveGamePopup _leaveGame;

		[SetUp]
		public void SetUp ()
		{
			LoggerConfigurator.ConfigureLogger ();
			LoggerConfigurator.ConfigureLogger ();
			var kernel = new StandardKernel (new UnityNinjectSettings (), new MainModule (), new UnityModule (),
											 new LateBoundModule ());
			var uiContext = kernel.Get<UiContext> ();
			_menuVm = uiContext.Menu;
			_gameVm = uiContext.GameViewModel;
			_resultVm = uiContext.ResultViewModel;
			_leaveGame = uiContext.LeaveGamePopup;

			_menuVm.Visible = true;
		}

		[Test]
		public void AiGameFull ()
		{
			Assert.True (_menuVm.Visible);
			Assert.False (_gameVm.Visible);
			Assert.False (_resultVm.Visible);

			_menuVm.ChooseAiModeBtn ();
			Assert.False (_menuVm.Visible);
			Assert.True (_gameVm.Visible);
			Assert.False (_resultVm.Visible);

			if (_gameVm.TotalCoins == 20) {//real player makes turn first
				GameTestUtils.AiGameRealPlayerMakesTurnFirst (_gameVm);
			} else {
				GameTestUtils.AiGameAiMakesTurnFirst (_gameVm);
			}

			//game is over
			Assert.False (_menuVm.Visible);
			Assert.False (_gameVm.Visible);
			Assert.True (_resultVm.Visible);

			Assert.True (_resultVm.Turns.Count > 0);
			Assert.True (_resultVm.WinnerId > 0);
			Assert.True (_resultVm.Visible);

			_resultVm.ContinueBtn ();

			Assert.True (_menuVm.Visible);
			Assert.False (_gameVm.Visible);
			Assert.False (_resultVm.Visible);
		}

		[Test]
		public void CheckTwoFullGames ()
		{
			AiGameFull ();
			AiGameFull ();
		}

		[Test]
		public void LeaveGame ()
		{
			_menuVm.ChooseAiModeBtn ();

			_gameVm.LeaveBtn ();

			_leaveGame.LeaveBtn ();

			Assert.True (_menuVm.Visible);
			Assert.False (_gameVm.Visible);
			Assert.False (_resultVm.Visible);
		}

		[Test]
		public void CancelLeaveGame ()
		{
			_menuVm.ChooseAiModeBtn ();

			_gameVm.LeaveBtn ();

			_leaveGame.ContinueBtn ();

			Assert.False (_menuVm.Visible);
			Assert.True (_gameVm.Visible);
			Assert.False (_resultVm.Visible);
		}
	}
}
