﻿namespace SimpleLogger.Logging.Formatters
{
	public class ShortLoggerFormatter : ILoggerFormatter
	{
		public string ApplyFormat(LogMessage logMessage)
		{
			return string.Format("{0}, [{1}]: {2}",logMessage.Level, logMessage.CallingClass,  logMessage.Text);
		}
	}
}