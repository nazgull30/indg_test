﻿using SimpleLogger.Logging;
using SimpleLogger.Logging.Formatters;
using UnityEngine;
using SimpleGelf.Core;
using System;
using System.Collections;
using System.Text;
using UniRx;
using System.Collections.Generic;

namespace SimpleGelf.GraylogLogger
{
	public class LogServerHandler : ILoggerHandler 
	{
		readonly GelfMessageSerializer _gelfSerializer = new GelfMessageSerializer();
		readonly UTF8Encoding encoding = new UTF8Encoding();
		readonly ILoggerFormatter _loggerFormatter;

		public LogServerHandler() : this(new ShortLoggerFormatter()) { }

		public LogServerHandler(ILoggerFormatter loggerFormatter)
		{
			_loggerFormatter = loggerFormatter;
		}

		string _buildVersion;
		string _deviceGeneration;
		string _deviceUniqueIdentifier;
		string _buildType;
		string _host;
		Dictionary<string, string> _headers;
		public LogServerHandler Configure(string host)
		{
			_host = host;
			var form = new WWWForm ();
			_headers = form.headers;
			_headers["Content-Type"] = "application/json";

			Observable.FromEvent<Application.LogCallback>(
				h => HandleLog,
				h => Application.logMessageReceived += h, 
				h => Application.logMessageReceived -= h);

			_buildVersion = Application.version;
#if UNITY_IOS
            _deviceGeneration = UnityEngine.iOS.Device.generation.ToString ();
#elif UNITY_ANDROID
            _deviceGeneration = "";
#endif
            _deviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier;

#if UNITY_IOS && !UNITY_EDITOR
			if (Application.version.Contains ("DEV2")) {
				_buildType = "DEV2";
			} if (Application.version.Contains ("DEV")) {
				_buildType = "DEV";
			} else if (Application.version.Contains ("STAGE")) {
				_buildType = "STAGE";
			} else {
				_buildType = "PROD";
			//Production server
			}
#endif

			return this;
		}

		int _userId;
		string _username;
		string _gcId;
		long _fbId;

		public void SetGameCenterId(string gcId)
		{
			_gcId = gcId;
		}

		public LogServerHandler SetUserInfo(int userId, string username, long fbId)
		{
			_userId = userId;
			_username = username;
			_fbId = fbId;
			return this;
		}

		void HandleLog(string logString, string stackTrace, LogType type) {
			if (type == LogType.Exception) {
				var mes = string.Format ("{0}: {1}", logString, stackTrace);
				var logMessage = new LogMessage (SimpleLogger.Logger.Level.Error, mes, DateTime.Now, "Application.logMessageReceived", "HandleLog", -1);
				Publish (logMessage);
			}
		}

		public void Publish(LogMessage logMessage)
		{
			if (string.IsNullOrEmpty (_host))
				return;

			var str = _loggerFormatter.ApplyFormat (logMessage);

			var builder = new GelfMessageBuilder (str, "Heroes Rage");

			var level = GelfLevel.Debug;
			switch (logMessage.Level) {
			case SimpleLogger.Logger.Level.Error:
				level = GelfLevel.Error;
				break;
			case SimpleLogger.Logger.Level.Warning:
				level = GelfLevel.Warning;
				break;
			case SimpleLogger.Logger.Level.Info:
				level = GelfLevel.Informational;
				break;
			case SimpleLogger.Logger.Level.Severe:
				level = GelfLevel.Critical;
				break;
			}

			builder.SetLevel (level);
			builder.SetTimestamp (DateTime.UtcNow);

			if (!string.IsNullOrEmpty (_buildType)) {
				builder.SetAdditionalField ("buildType", _buildType);
			}

			if (_userId != 0) {
				builder.SetAdditionalField ("userId", _userId.ToString ());
			}

			if (!string.IsNullOrEmpty (_username)) {
				builder.SetAdditionalField ("user_name", _username);
			}

			if (!string.IsNullOrEmpty (_gcId)) {
				builder.SetAdditionalField ("gameCenterId", _gcId);
			}

			if (_fbId != 0) {
				builder.SetAdditionalField ("facebookId", _fbId.ToString ());
			}

			if (!string.IsNullOrEmpty (_buildVersion)) {
				builder.SetAdditionalField ("build", _buildVersion);
			}
				
			if (!string.IsNullOrEmpty (_deviceGeneration)) {
				builder.SetAdditionalField ("device_generation", _deviceGeneration);
			}

			if (!string.IsNullOrEmpty (_deviceUniqueIdentifier)) {
				builder.SetAdditionalField ("device_id", _deviceUniqueIdentifier);
			}

			if (!string.IsNullOrEmpty (logMessage.CallingClass)) {
				builder.SetAdditionalField ("calling_class", logMessage.CallingClass);
			}

			if (!string.IsNullOrEmpty (logMessage.CallingMethod)) {
				builder.SetAdditionalField ("calling_method", logMessage.CallingMethod);
			}
			var mes = _gelfSerializer.Serialize (builder.ToMessage ());
			PushMessage (_host, mes);

		}

		void PushMessage (string url, string mes)
		{
			Observable.FromCoroutine<string>((observer, cancellationToken) => PushMessage(url, encoding.GetBytes (mes), observer, cancellationToken))
				.Subscribe (m => Debug.Log ("Logserver, push: " + mes), ex =>  Debug.LogError ("Logserver, ex = " + ex));
		}

		IEnumerator PushMessage(string url, byte[] postData, UniRx.IObserver<string> observer, CancellationToken cancellationToken)
		{
			var www = new WWW(url, postData);
			while (!www.isDone && !cancellationToken.IsCancellationRequested)
			{
				yield return null;
			}

			if (cancellationToken.IsCancellationRequested) yield break;

			if (www.error != null)
			{
				observer.OnError(new Exception(www.error));
			}
			else
			{
				observer.OnNext(www.text);
			}
		}

	}
}

