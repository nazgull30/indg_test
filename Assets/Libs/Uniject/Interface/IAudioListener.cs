using System;

namespace Uniject {
    public interface IAudioListener {
        void noOp();
        ITestableGameObject Obj { get; }
    }
}

