﻿using UnityEngine;
using System.Collections;

public class NguiLocalRotationBinding : NguiBinding 
{

	private EZData.Property<Quaternion> _rotation;
	Transform _cachedTr;

	public override void Awake ()
	{
		base.Awake ();

		_cachedTr = GetComponent<Transform> ();
	}

	protected override void Unbind ()
	{
		base.Unbind ();

		if (_rotation != null) {
			_rotation.OnChange -= OnChange;
			_rotation = null;
		}
	}

	protected override void Bind ()
	{
		base.Bind ();

		var context = GetContext (Path);
		if (context == null) {
			Debug.LogWarning ("NguiRotation.UpdateBinding - context is null");
			return;
		}

		_rotation = context.FindProperty<Quaternion> (Path, this);

		if (_rotation != null) {
			_rotation.OnChange += OnChange;
		}
	}

	protected override void OnChange ()
	{
		base.OnChange ();

		if (_rotation != null) {
			_cachedTr.localRotation = _rotation.GetValue ();
		}
	}
}
