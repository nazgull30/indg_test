﻿using UnityEngine;
using System.Collections;

public class NguiRotationBinding : NguiBinding 
{

	private EZData.Property<Quaternion> _rotation;
	private EZData.Property<Vector3> _eulerAngles;
	Transform _cachedTr;

	public override void Awake ()
	{
		base.Awake ();

		_cachedTr = GetComponent<Transform> ();
	}

	protected override void Unbind ()
	{
		base.Unbind ();

		if (_rotation != null) {
			_rotation.OnChange -= OnChange;
			_rotation = null;
		}
		if (_eulerAngles != null) {
			_eulerAngles.OnChange -= OnChange;
			_eulerAngles = null;
		}
	}

	protected override void Bind ()
	{
		base.Bind ();

		var context = GetContext (Path);
		if (context == null) {
			Debug.LogWarning ("NguiRotation.UpdateBinding - context is null");
			return;
		}


		_rotation = context.FindProperty<Quaternion> (Path, this);
		_eulerAngles = context.FindProperty<Vector3> (Path, this);

		if (_rotation != null) {
			_rotation.OnChange += OnChange;
		}

		if (_eulerAngles != null) {
			_eulerAngles.OnChange += OnChange;
		}

	}

	protected override void OnChange ()
	{
		base.OnChange ();

		if (_rotation != null) {
			_cachedTr.rotation = _rotation.GetValue ();
		}
		if (_eulerAngles != null) {
			_cachedTr.eulerAngles = _eulerAngles.GetValue ();
		}
	}
}
