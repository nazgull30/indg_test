using UnityEngine;
using System;
using UniRx;

[System.Serializable]
[AddComponentMenu("NGUI/NData/Slider Binding")]
public class NguiSliderBinding : NguiPollingCustomBoundsNumericBinding
{
	private UnityEngine.UI.Slider _slider;
    private IDisposable _disableDisposable;
	public override void Awake()
	{
		base.Awake();
		_slider = GetComponent<UnityEngine.UI.Slider> ();

        if (_slider != null)
        {
            _slider.onValueChanged.AddListener(new UnityEngine.Events.UnityAction<float>(_ =>
            {
                EnableUpdate = true;
                if (_disableDisposable == null)
                    _disableDisposable = Observable.IntervalFrame(30).Where(v => !_slider.enabled).Subscribe(__ =>
                    {
                        EnableUpdate = false;
                        _disableDisposable.Dispose();
                        _disableDisposable = null;
                    });
            }));
        }
	}
	
	protected override double GetValue()
	{

		if (_slider != null)
			return _slider.value;

		return 0;
	}
	
	protected override void SetValue(double val)
	{
		if (_slider != null)
		{
			_slider.value = (float)val;
		}
	}

    void OnDestroy()
    {
        if (_disableDisposable != null)
            _disableDisposable.Dispose();
    }

}
