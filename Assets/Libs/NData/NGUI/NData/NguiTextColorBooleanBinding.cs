using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
[AddComponentMenu("NGUI/NData/Text Color Boolean Binding")]
public class NguiTextColorBooleanBinding : NguiBooleanBinding
{
	public Color32 trueColor;
	public Color32 falseColor;

	Text _text;
	public override void Awake()
	{
		base.Awake();
		_text = GetComponent<Text> ();
	}
	
	protected override void ApplyNewValue(bool newValue)
	{
		if (_text == null)
			return;

		_text.color = newValue ? trueColor : falseColor;
	}

}
