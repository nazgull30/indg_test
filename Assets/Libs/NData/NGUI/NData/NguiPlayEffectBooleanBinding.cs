using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
[AddComponentMenu("NGUI/NData/Play Effect Boolean Binding")]
public class NguiPlayEffectBooleanBinding : NguiBooleanBinding
{
	protected ParticleSystem[] _particles;

    public override void Awake()
    {
        base.Awake();
        _particles = GetComponentsInChildren<ParticleSystem>();
    }
	
	protected override void ApplyNewValue(bool newValue)
	{
        foreach (var particle in _particles)
        {
            if (newValue)
            {
                particle.Play();
            }
            else
            {
                particle.Stop();
                particle.Clear();
            }
        }
	}

}
