﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UniRx;

[System.Serializable]
[AddComponentMenu("NGUI/NData/OnDragableClick Binding")]
public class NguiOnDragableClickBinding : NguiCommandBinding, IPointerDownHandler, IPointerUpHandler
{
	Button _button;
	public override void Awake()
	{
		base.Awake();
		var _selectable = GetComponent<Selectable> ();
		if (_selectable == null) {
			_selectable = gameObject.AddComponent<Button>();
			_selectable.interactable = true;
			_selectable.transition = Selectable.Transition.None;
		}

		_button = _selectable as Button;
	}

	IDisposable _disposable = Disposable.Empty;
	public void OnPointerDown(PointerEventData eventData)
	{
		_timeTillInvoke = 0f;
		_disposable = Observable.EveryUpdate ().Subscribe (_ => {
			Tick();
		});
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (_timeTillInvoke <= TIME_WHILE_INVOKE_POSSIBLE && eventData.position == eventData.pressPosition)
		{
			InvokeCommand ();
		}
		_disposable.Dispose ();
	}

	float TIME_WHILE_INVOKE_POSSIBLE = 0.1f;
	float _timeTillInvoke = 0f;
	void Tick()
	{
		_timeTillInvoke += Time.deltaTime;
	}

	void InvokeCommand()
	{
		if (_command == null)
		{
			return;
		}

		_command.DynamicInvoke();
	}

	public void InternalClick()
	{
		InvokeCommand ();
	}
}
