using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
[AddComponentMenu("NGUI/NData/List Item Template")]
public class NguiListItemTemplate : MonoBehaviour
{
	public GameObject Template;

    Transform tr;
    void Awake()
    {
        tr = transform;
    }

	public GameObject Instantiate(EZData.Context itemContext, int index, int groupNumber)
	{
		if (Template == null)
			return null;		

		GameObject instance;
        NguiItemDataContext itemData;
        
        instance = (GameObject)Instantiate(Template);
        itemData = instance.AddComponent<NguiItemDataContext>();

        var subTemplates = instance.GetComponentsInChildren<NguiListItemTemplate>();
        foreach (var st in subTemplates)
        {
            if (st.Template == instance)
                st.Template = Template;
        }

    
        
        instance.transform.SetParent(tr);
		itemData.SetContext(itemContext);;		
		itemData.SetIndex(index);

		//if (retinaProUtil.sharedInstance != null)
		//	retinaProUtil.sharedInstance.refreshVisible (instance);

        return instance;
	}

}
