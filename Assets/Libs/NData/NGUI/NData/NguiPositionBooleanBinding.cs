using UnityEngine;


[System.Serializable]
[AddComponentMenu("NGUI/NData/Position Boolean Binding")]
public class NguiPositionBooleanBinding : NguiBooleanBinding
{
    public float trueX;
    public float trueY;
    public float falseX;
    public float falseY;

	RectTransform _rectTr;
	public override void Awake()
	{
		base.Awake();
        _rectTr = GetComponent<RectTransform>();
	}
	
	protected override void ApplyNewValue(bool newValue)
	{
        if (_rectTr == null)
			return;

        _rectTr.anchoredPosition = newValue ? new Vector2(trueX, trueY) : new Vector2(falseX, falseY);
	}

}
