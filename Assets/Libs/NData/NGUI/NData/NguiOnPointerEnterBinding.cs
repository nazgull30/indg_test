﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UniRx;

[System.Serializable]
[AddComponentMenu("NGUI/NData/OnPointerEnter Binding")]
public class NguiOnPointerEnterBinding : NguiCommandBinding, IPointerEnterHandler
{
	public void OnPointerEnter(PointerEventData eventData)
	{
		if (_command == null)
		{
			return;
		}

		_command.DynamicInvoke();
	}
}
