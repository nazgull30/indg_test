using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
[AddComponentMenu("NGUI/NData/Graphic Color Boolean Binding")]
public class NguiGraphicColorBooleanBinding : NguiBooleanBinding
{
	public Color32 trueColor;
	public Color32 falseColor;
    public Color32 shadowTrueColor;
    public Color32 shadowFalseColor;

    UnityEngine.UI.Graphic _graphic;
    UnityEngine.UI.Shadow _shadow;	
	public override void Awake()
	{
		base.Awake();
        _graphic = GetComponent<UnityEngine.UI.Graphic>();
        _shadow = GetComponent<UnityEngine.UI.Shadow>();
	}
	
	protected override void ApplyNewValue(bool newValue)
	{
        if (_graphic != null)
			_graphic.color = newValue ? trueColor : falseColor;

        if (_shadow != null)
            _shadow.effectColor = newValue ? shadowTrueColor : shadowFalseColor;
	}

}
