﻿using UnityEngine;

[System.Serializable]
[AddComponentMenu("NGUI/NData/Transform Binding")]
public class NguiTransformBinding : NguiBinding 
{
	private EZData.Property<Transform> _transformParameter;

    Transform _transform;

	public override void Awake()
	{
		base.Awake ();

		_transform = this.transform;
	}

	protected override void Unbind()
	{
		base.Unbind();

		if (_transformParameter != null)
		{
			_transformParameter.OnChange -= OnChange;
			_transformParameter = null;
		}
	}

	protected override void Bind()
	{
		base.Bind();

		var context = GetContext(Path);
		if (context == null)
		{
			Debug.LogWarning("UpdateBinding >>> context is NULL");
			return;
		}

		_transformParameter = context.FindProperty<Transform>(Path, this);

		if (_transformParameter != null)
		{
			_transformParameter.OnChange += OnChange;
			if (_transform != null)
				_transformParameter.SetValue (_transform);
		}
	}


	protected override void OnChange()
	{
		
	}
}
