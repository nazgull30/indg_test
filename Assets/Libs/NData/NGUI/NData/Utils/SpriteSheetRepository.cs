﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpriteSheetRepository
{
	#region SINGLETON

	static SpriteSheetRepository _instance;
	public static SpriteSheetRepository Instance { 
		get {
			if (_instance == null)
				_instance = new SpriteSheetRepository ();
			return _instance;
		}
	}

	#endregion

	readonly Dictionary<string, List<Sprite>> _sheets = new Dictionary<string, List<Sprite>>();

	public Sprite GetSpriteByName(string spriteName, string spriteSheet, bool isFolder = false, string assetBundle = "")
	{
		if (string.IsNullOrEmpty (spriteName) || string.IsNullOrEmpty (spriteSheet))
			return null;

		Sprite sprite = null;


		if (!_sheets.ContainsKey (spriteSheet)) {
			var sprites = Resources.LoadAll<Sprite> (spriteSheet);
			if(sprites.Length == 0){
#if ASSETBUNDLES
			if(string.IsNullOrEmpty (assetBundle)) {
				sprites = AssetBundleService.Instance.LoadUIAssetWithSubAssets<Sprite> (spriteSheet);
			} else {
				sprites = AssetBundleService.Instance.LoadUIAssetWithSubAssets<Sprite> (assetBundle, spriteSheet);
			}
			#else
				if(!isFolder) {
					sprites = Resources.LoadAll <Sprite> (spriteSheet);
				} else {
					return Resources.Load <Sprite> (spriteSheet);
				}
#endif
			}

			if (sprites.Length > 0) {
				_sheets.Add (spriteSheet, sprites.ToList ());
				sprite = _sheets [spriteSheet].Find (s => s.name.Equals (spriteName));
			}

		} else {
			sprite = _sheets[spriteSheet].Find (s => s.name.Equals (spriteName));
		}
		return sprite;
	}

	public List<Sprite> GetAllSpritesFromSheet(string spriteSheet)
	{
		List<Sprite> result = new List<Sprite>();
		if (spriteSheet == string.Empty) return result;

		if (!_sheets.ContainsKey(spriteSheet))
		{
			var sprites = Resources.LoadAll<Sprite>(spriteSheet);
			if (sprites.Length == 0) {				
#if ASSETBUNDLES
			sprites = AssetBundleService.Instance.LoadUIAssetWithSubAssets<Sprite> (spriteSheet);
			#else
				sprites = Resources.LoadAll <Sprite> (spriteSheet);
#endif
			}
			if (sprites.Length > 0) {
				result = sprites.ToList ();
				_sheets.Add (spriteSheet, result);
			}
		} else {
			result = _sheets [spriteSheet];
		}

		return result;

	}

	public void Clear ()
	{
		_sheets.Clear ();
		_instance = null;
	}

}
