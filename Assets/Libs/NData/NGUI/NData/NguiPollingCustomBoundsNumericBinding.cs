using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;

[System.Serializable]
public abstract class NguiPollingCustomBoundsNumericBinding : NguiCustomBoundsNumericBinding
{
	public NguiBindingDirection Direction = NguiBindingDirection.TwoWay;
	public NguiBindingInitialValue InitialValue = NguiBindingInitialValue.TakeFromModel;
	
	private double _prevValue;
	private bool _inited;
	
	public override void Start()
	{
		base.Start();
		
		if (InitialValue == NguiBindingInitialValue.TakeFromView)
		{
			_inited = true;
			_prevValue = GetValue();
			SetCustomBoundsNumericValue(_prevValue);
		}

        EnableUpdate = true;
	}

    IDisposable _updateDisposable;
    protected bool EnableUpdate
    {
        get { return _updateDisposable != null; }
        set
        {
            if (value)
            {
                if (_updateDisposable == null)
                    _updateDisposable = Observable.EveryUpdate().Where(v => Direction.NeedsViewTracking() && _prevValue != GetValue()).Subscribe(_ => InternalUpdate());			
            }
            else
            if (_updateDisposable != null)
            {
                _updateDisposable.Dispose();
                _updateDisposable = null;                
            }            
        }
    }

	void InternalUpdate()
	{
        var newScale = GetValue();
		if (_prevValue != newScale)
		{
            _prevValue = newScale;			
            SetCustomBoundsNumericValue(newScale);            
		}
	}
	
	protected sealed override void ApplyNewCustomBoundsValue(double val)
	{
        if (!_inited && InitialValue == NguiBindingInitialValue.TakeFromView)
			return;
		
		_inited = true;
		if (Direction.NeedsViewUpdate())
		{
            _prevValue = val;
			SetValue(val);            
		}
	}

	void OnDestroy ()
	{
		if (_updateDisposable != null) {
			_updateDisposable.Dispose ();
		}
	}
	
	protected abstract double GetValue();
	protected abstract void SetValue(double val);
}
