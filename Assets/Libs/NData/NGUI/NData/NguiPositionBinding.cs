using UnityEngine;

[System.Serializable]
[AddComponentMenu("NGUI/NData/Position Binding")]
public class NguiPositionBinding : NguiPollingVector3Binding
{
		
	Transform _cachedTr;
	public override void Awake ()
	{
		base.Awake ();
		_cachedTr = transform;
	}

	protected override Vector3 GetValue()
	{
		return _cachedTr.position;
	}
	
	protected override void SetValue(Vector3 val)
	{
		_cachedTr.position = val;
	}
}
