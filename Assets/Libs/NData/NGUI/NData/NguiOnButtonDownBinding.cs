﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UniRx;

[System.Serializable]
[AddComponentMenu("NGUI/NData/OnButtonDown Binding")]
public class NguiOnButtonDownBinding : NguiCommandBinding, IPointerDownHandler, IPointerUpHandler
{
	Button _button;
	public override void Awake()
	{
		base.Awake();
		var _selectable = GetComponent<Selectable> ();
		if (_selectable == null) {
			_selectable = gameObject.AddComponent<Button>();
			_selectable.interactable = true;
			_selectable.transition = Selectable.Transition.None;
		}

		_button = _selectable as Button;
	}
		
	bool _isClicked = false;
	IDisposable _disposable = Disposable.Empty;
	public void OnPointerDown(PointerEventData eventData)
	{
		_isClicked = false;
		_timeTillInvoke = 0f;
		_disposable = Observable.EveryUpdate ().Subscribe (_ => {
			Tick(eventData);
		});
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		TickPosition (eventData);
		_timeTillInvoke = 0f;
		_disposable.Dispose ();
	}

	float TIME_TILL_INVOKE_STARTED = 0.1f;
	float _timeTillInvoke = 0f;
	void Tick(PointerEventData eventData)
	{
		TickTime ();
		TickPosition (eventData);
	}

	void TickTime()
	{
		_timeTillInvoke += Time.deltaTime;
		if (_timeTillInvoke >= TIME_TILL_INVOKE_STARTED)
		{
			InvokeCommand ();
		}
	}

	void TickPosition(PointerEventData eventData)
	{
		if(eventData.position != eventData.pressPosition)
		{
			InvokeCommand ();
		}
	}

	void InvokeCommand()
	{
		if (_isClicked)
			return;
		
		_isClicked = true;
		_disposable.Dispose ();
		if (_command == null)
		{
			return;
		}
		_command.DynamicInvoke();
	}

}
