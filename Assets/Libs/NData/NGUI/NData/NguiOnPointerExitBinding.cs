﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UniRx;

[System.Serializable]
[AddComponentMenu("NGUI/NData/OnPointerExit Binding")]
public class NguiOnPointerExitBinding : NguiCommandBinding, IPointerExitHandler
{
	public void OnPointerExit(PointerEventData eventData)
	{
		if (_command == null)
		{
			return;
		}

		_command.DynamicInvoke();
	}
}
